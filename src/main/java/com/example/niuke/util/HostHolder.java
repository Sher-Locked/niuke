package com.example.niuke.util;

import com.example.niuke.entity.User;
import org.springframework.stereotype.Component;

/**
 * 持有用户信息，代替session, 防止用户冲突
 */
@Component
public class HostHolder {
    /**
     * Auther sun
     * DATE 2022/4/16 10:56
     * VERSION 1.0
     */

    private ThreadLocal<User> users = new ThreadLocal<>();

    public void setUser(User user) {
        users.set(user);
    }

    public User getUser() {
        return users.get();
    }

    public void clear() {
        users.remove();
    }
}
