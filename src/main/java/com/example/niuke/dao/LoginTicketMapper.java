package com.example.niuke.dao;

import com.example.niuke.entity.LoginTicket;
import org.apache.ibatis.annotations.*;

@Mapper
@Deprecated
public interface LoginTicketMapper {
    /**
     * Auther sun
     * DATE 2022/4/16 8:44
     * VERSION 1.0
     */

    @Insert({
            "insert into niu_ke.login_ticket(user_id, ticket, status, expired) ",
            "values (#{userId}, #{ticket}, #{status}, #{expired});"
    })
    @Options(useGeneratedKeys = true,keyProperty = "id")
    int insertLoginTicket(LoginTicket loginTicket);

    @Select({
            "select id,user_id, ticket, status, expired ",
            "from niu_ke.login_ticket where ticket=#{ticket};"
    })
    LoginTicket selectByTicket(String ticket);

    @Update({
            "update niu_ke.login_ticket set status=#{status} where ticket=#{ticket};"
    })
    int updateStatus(String ticket, int status);
}
