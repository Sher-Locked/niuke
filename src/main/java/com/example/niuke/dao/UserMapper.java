package com.example.niuke.dao;

import com.example.niuke.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {
    User selectById(int id);
    User selectByName(String name);
    User selectByEmail(String email);

    int insertUser(User user);

    int updateStatus(int id, int status);
    int updateHeader(int id, String url);
    int updatePassword(int id, String password);

    void selectByName();
}
