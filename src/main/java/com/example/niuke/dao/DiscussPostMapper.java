package com.example.niuke.dao;

import com.example.niuke.entity.DiscussPost;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DiscussPostMapper {
    //考虑分页
    List<DiscussPost> selectDiscussPosts(int userId, int offset, int limit);


    int selectDiscussPostRows(int userId);

    int insertDiscussPost(DiscussPost discussPost);

    //查询帖子详情
    DiscussPost selectDiscussPostById(int id);

    int updateCommentCount(int id, int commentCount);

    int updateType(int id, int type);

    int updateStatus(int id, int status);
}
