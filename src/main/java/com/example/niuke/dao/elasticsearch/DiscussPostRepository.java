package com.example.niuke.dao.elasticsearch;

import com.example.niuke.entity.DiscussPost;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiscussPostRepository extends ElasticsearchRepository<DiscussPost, Integer> {
    /**
     * Auther sun
     * DATE 2022/4/21 21:25
     * VERSION 1.0
     */


}
