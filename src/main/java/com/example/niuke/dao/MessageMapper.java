package com.example.niuke.dao;

import com.example.niuke.entity.Message;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MessageMapper {
    /**
     * Auther sun
     * DATE 2022/4/17 15:12
     * VERSION 1.0
     */

    List<Message> selectConversations(int userId, int offset, int limit);

    int selectConversationCount(int userId);

    List<Message> selectLetters(String conversationId, int offset, int limit);

    int selectLettersCount(String conversation);

    int selectLetterUnreadCount(int userId, String conversationId);

    /**
     * 新增消息
     * @return
     */
    int insertMessage(Message message);

    int updateStatus(List<Integer> ids, int status);

    //查询某个主题下最新的通知
    Message selectLatestNotice(int userId, String topic);
    //查询某个主题所包含的通知的数量
    int selectNoticeCount(int userId, String topic);
    //查询未读的通知的数量
    int selectNoticeUnReadCount(int userId, String topic);

    //查询某个主题包含的通知列表
    List<Message> selectNotices(int userId, String topic, int offset, int limit);


}
