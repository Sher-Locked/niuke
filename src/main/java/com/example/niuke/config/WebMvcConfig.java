package com.example.niuke.config;

import com.example.niuke.interceptor.LoginRequiredInterceptor;
import com.example.niuke.interceptor.LoginTicketInterceptor;
import com.example.niuke.interceptor.MessageInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    /**
     * Auther sun
     * DATE 2022/4/16 10:43
     * VERSION 1.0
     */
    @Resource
    private LoginTicketInterceptor loginTicketInterceptor;
    //因为spring security
    /*@Resource
    private LoginRequiredInterceptor loginRequiredInterceptor;*/
    @Resource
    private MessageInterceptor messageInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginTicketInterceptor);

        //registry.addInterceptor(loginRequiredInterceptor);

        registry.addInterceptor(messageInterceptor);
    }
}
