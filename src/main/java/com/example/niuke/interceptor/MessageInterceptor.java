package com.example.niuke.interceptor;

import com.example.niuke.entity.User;
import com.example.niuke.service.MessageService;
import com.example.niuke.util.HostHolder;
import org.apache.tomcat.util.net.AbstractEndpoint;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@Component
public class MessageInterceptor implements HandlerInterceptor {
    /**
     * Auther sun
     * DATE 2022/4/20 13:14
     * VERSION 1.0
     */

    @Resource
    private HostHolder hostHolder;
    @Resource
    private MessageService messageService;

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        User user = hostHolder.getUser();
        if (user!=null && modelAndView!=null) {
            int letterUnreadCount = messageService.findLetterUnreadCount(user.getId(), null);
            int noticeUnreadCount = messageService.findNoticeUnreadCount(user.getId(), null);
            modelAndView.addObject("allUnreadCount", letterUnreadCount+noticeUnreadCount);
        }
    }
}
