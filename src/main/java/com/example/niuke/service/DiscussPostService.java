package com.example.niuke.service;

import com.example.niuke.dao.DiscussPostMapper;
import com.example.niuke.entity.DiscussPost;
import com.example.niuke.util.SensitiveFilter;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DiscussPostService {
    /**
     * Auther sun
     * DATE 2022/4/14 22:00
     * VERSION 1.0
     */

    @Resource
    private DiscussPostMapper discussPostMapper;
    @Resource
    private SensitiveFilter sensitiveFilter;



    public List<DiscussPost> findDiscussPosts(int userId, int offset, int limit){
        return discussPostMapper.selectDiscussPosts(userId, offset, limit);
    }

    public int selectDiscussPostRows(int userId) {
        return discussPostMapper.selectDiscussPostRows(userId);
    }

    public int addDiscussPost(DiscussPost discussPost){
        if(discussPost == null) {
            throw new IllegalArgumentException("参数不能为空");
        }

        discussPost.setTitle(HtmlUtils.htmlEscape(discussPost.getTitle()));//防止注入
        discussPost.setContent(HtmlUtils.htmlEscape(discussPost.getContent()));//防止注入

        discussPost.setTitle(sensitiveFilter.filter(discussPost.getTitle()));
        discussPost.setContent(sensitiveFilter.filter(discussPost.getContent()));

        return discussPostMapper.insertDiscussPost(discussPost);
    }

    public DiscussPost findDiscussPostById(int id){
        return discussPostMapper.selectDiscussPostById(id);
    }
    public int updatePostCount(int id, int count) {
        return discussPostMapper.updateCommentCount(id, count);
    }


    public int updateType(int id, int type) {
        return discussPostMapper.updateType(id, type);
    }
    public int updateStatus(int id, int type) {
        return discussPostMapper.updateStatus(id, type);
    }

}


