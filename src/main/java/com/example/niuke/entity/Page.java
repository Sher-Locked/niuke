package com.example.niuke.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Page {
    /**
     * Auther sun
     * DATE 2022/4/14 23:11
     * VERSION 1.0
     */
    //当前页码
    private int current = 1;
    //显示上限
    private int limit = 6;
    //数据总数
    private int rows;
    //查询路径
    private String path;

    public void setCurrent(int current) {
        if(current>=1){
            this.current = current;
        }
    }

    public void setLimit(int limit){
        if (limit >=1 && limit<=100){
            this.limit = limit;
        }
    }
    public void setRows(int rows){
        if(rows>=0){
            this.rows = rows;
        }
    }
    //从那开始
    public int getOffset(){
        return (current-1) * limit;
    }
    //总页数
    public int getTotal(){
        if (rows % limit == 0){
            return rows / limit;
        }else {
            return rows / limit + 1;
        }
    }
    //起始页码
    public int getFrom(){
        int from = current - 2;
        return from < 1 ? 1 : from;
    }
    //结束页
    public int getTo(){
        int to = current + 2;
        int total = getTotal();
        return to > total ? total : to;
    }
}
