package com.example.niuke.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginTicket {
    /**
     * Auther sun
     * DATE 2022/4/16 8:42
     * VERSION 1.0
     */

    private int id;
    private int userId;
    private String ticket;
    private int status;//0有效
    private Date expired;//过期时间
}
