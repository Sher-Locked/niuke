package com.example.niuke.event;

import com.alibaba.fastjson.JSONObject;
import com.example.niuke.entity.Event;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class EventProducer {
    /**
     * Auther sun
     * DATE 2022/4/19 19:35
     * VERSION 1.0
     */
    @Resource
    private KafkaTemplate kafkaTemplate;

    // 处理事件
    public void fireEvent(Event event) {
        //将事件发布到指定的主题
        kafkaTemplate.send(event.getTopic(), JSONObject.toJSONString(event));
    }
}
